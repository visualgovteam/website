﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="VisualGov.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <!-- SLIDER -->
            <div class="span-24 notopmargin">
                <div id="va-accordion" class="va-container slider-area">
                    <div class="va-nav">
                        <span class="va-nav-prev">Previous</span>
                        <span class="va-nav-next">Next</span>
                    </div>
                    <div class="va-wrapper">
                        <div class="va-slice va-slice-1">
                            <h3 class="va-title">Windows 8 UI Design & Development</h3>
                            <div class="va-content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br/><br/>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
                                <a href="#" class="va-more"></a>
                            </div>
                        </div>
                        <div class="va-slice va-slice-2">
                            <h3 class="va-title">Payment Processing Solutions</h3>
                            <div class="va-content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br/><br/>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
                                <a href="#" class="va-more"></a>
                            </div>	
                        </div>
                        <div class="va-slice va-slice-3">
                            <h3 class="va-title">IVR (Interactive Voice Response)</h3>
                            <div class="va-content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br/><br/>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
                                <a href="#" class="va-more"></a>
                            </div>	
                        </div>
                        <div class="va-slice va-slice-4">
                            <h3 class="va-title">Azure Consulting</h3>
                            <div class="va-content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br/><br/>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
                                <a href="#" class="va-more"></a>
                            </div>	
                        </div>
                        <div class="va-slice va-slice-5">
                            <h3 class="va-title">Windows Phone Development</h3>
                            <div class="va-content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br/><br/>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
                                <a href="#" class="va-more"></a>
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SLIDER -->
      <!-- MAIN CONTENT -->
	  		<div class="slogan">
                <div class="span-24">
                	<div class="span-18 notopmargin">
                    	<h1>Some words about <a class="link" href="">VisualGov</a></h1>
                		<p>VisualGov is a Microsoft Partner specialinzing in software and payment processing solution and services for governemnt. <a class="link" href="http://www.twitter.com/visualgov">Follow VisualGov</a> to be notified for future updates!</p>
                    </div>
                    
                </div>
            </div>
			<div class="span-24">
                <div id="ca-container" class="ca-container">
                	<div class="ca-wrapper home">
                  		<div class="ca-item ca-item-1">
                            <div class="ca-item-main">
                                <div class="new-item"></div>
                                <div class="logo">
                                    <img src="images/ca-item-1.png" title="Logo" alt="Logo" />
                                </div>
                                <div class="title">
                                    <h3>Windows 8 Marketplace</h3>
                                    <p>we are happy to announce we have the first app published in the Government section of the Microsoft App Store.</p>
                                </div>
                                <a href="#" class="ca-more"></a>
                            </div> 
                            <div class="ca-content-wrapper">
                                <div class="ca-content">
                                    <a href="#" class="ca-close">close</a>
                                    <div class="span-6 item"><a href="images/slide-1-1.jpg" class="item-preview" rel="prettyPhoto" title=""><img class="img-preview"  src="images/gallery/metric.jpg" alt=" "/></a></div>
                                    <h4>METRIC - HTML/CSS Template</h4>
                                    <div class="span-4 notopmargin">
                                        <ul>
                                            <li>40 Valid Pages</li>
                                            <li>7 Portfolio Pages</li>
                                            <li>Awesome Sliders</li>
                                            <li>20 Patterns</li>
                                        </ul>
                                    </div>
                                    <div class="span-4 notopmargin last">
                                        <ul>
                                            <li>243 icons</li>
                                            <li>Pricing Boxes</li>
                                            <li>Working Google Maps</li>
                                            <li>JQuery Dropdown</li>
                                        </ul>
                                    </div>
                                    <div class="span-4">
                                        <p class="price nobottommargin">$15</p>
                                    </div>
                                    <div class="span-4 purchase last">
                                        <div class="live">
                                            <a href="http://themeforest.net/item/metric-premium-html-template/full_screen_preview/669886" target="_blank">Live Preview</a>
                                        </div>
                                        <div class="buy">
                                            <a href="http://themeforest.net/item/metric-premium-html-template/669886?ref=OrangeIdea" target="_blank">Purchase on TF</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  		<div class="ca-item ca-item-2">
                            <div class="ca-item-main">
                                <div class="featured"></div>
                                <div class="logo">
                                    <img src="images/ca-item-2.png" title="Logo" alt="Logo" />
                                </div>
                                <div class="title">
                                    <h3>Paymenting Prcoessing</h3>
                                    <p>VisualGov offers a variaty of payment processiong soltuons for collecting payments online, on the phone or in the office. Contact us today to see what we can do for your agency!</p>
                                </div>
                                <a href="#" class="ca-more"></a>
                            </div>
                            <div class="ca-content-wrapper">
                                <div class="ca-content">
                                    <a href="#" class="ca-close">close</a>
                                    <div class="span-6 item"><a href="images/slide-1-1.jpg" class="item-preview" rel="prettyPhoto" title=""><img class="img-preview"  src="images/gallery/drop.jpg" alt=" "/></a></div>
                                    <h4>ANGRY DROP - HTML/CSS Template</h4>
                                    <div class="span-4 notopmargin">
                                        <ul>
                                            <li>40 Valid Pages</li>
                                            <li>7 Portfolio Pages</li>
                                            <li>Awesome Sliders</li>
                                            <li>20 Patterns</li>
                                        </ul>
                                    </div>
                                    <div class="span-4 notopmargin last">
                                        <ul>
                                            <li>243 icons</li>
                                            <li>Pricing Boxes</li>
                                            <li>Working Google Maps</li>
                                            <li>JQuery Dropdown</li>
                                        </ul>
                                    </div>
                                    <div class="span-4">
                                        <p class="price nobottommargin">$15</p>
                                    </div>
                                    <div class="span-4 purchase last">
                                        <div class="live">
                                            <a href="http://themeforest.net/item/angry-drop-premium-html-template/full_screen_preview/576234" target="_blank">Live Preview</a>
                                        </div>
                                        <div class="buy">
                                            <a href="http://themeforest.net/item/angry-drop-premium-html-template/576234?ref=OrangeIdea" target="_blank">Purchase on TF</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  		<div class="ca-item ca-item-3">
                            <div class="ca-item-main">
                                <div class="featured"></div>
                                <div class="logo">
                                    <img src="images/ca-item-3.png" title="Logo" alt="Logo" />
                                </div>
                                <div class="title">
                                    <h3>Windows Azure</h3>
                                    <p>Are you in the cloud? Its confusing and we have answers. Let VisualGov help you define and deploy your cloud stradagy.</p>
                                </div>
                                <a href="#" class="ca-more"></a>
                            </div>
                            <div class="ca-content-wrapper">
                                <div class="ca-content">
                                    <a href="#" class="ca-close">close</a>
                                    <div class="span-6 item"><a href="images/slide-1-1.jpg" class="item-preview" rel="prettyPhoto" title=""><img class="img-preview"  src="images/gallery/soul.jpg" alt=" "/></a></div>
                                    <h4>SOUL - HTML/CSS Template</h4>
                                    <div class="span-4 notopmargin">
                                        <ul>
                                            <li>40 Valid Pages</li>
                                            <li>7 Portfolio Pages</li>
                                            <li>Awesome Sliders</li>
                                            <li>20 Patterns</li>
                                        </ul>
                                    </div>
                                    <div class="span-4 notopmargin last">
                                        <ul>
                                            <li>243 icons</li>
                                            <li>Pricing Boxes</li>
                                            <li>Working Google Maps</li>
                                            <li>JQuery Dropdown</li>
                                        </ul>
                                    </div>
                                    <div class="span-4">
                                        <p class="price nobottommargin">$15</p>
                                    </div>
                                    <div class="span-4 purchase last">
                                        <div class="live">
                                            <a href="http://themeforest.net/item/soul-premium-html-template/full_screen_preview/515212" target="_blank">Live Preview</a>
                                        </div>
                                        <div class="buy">
                                            <a href="http://themeforest.net/item/soul-premium-html-template/515212?ref=OrangeIdea" target="_blank">Purchase on TF</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <div class="span-24 separator"></div>
			<div class="span-24 main-service">
            	<div class="span-12 notopmargin">
                    <h3>Check our <a href="" class="link">services</a> with really low prices</h3>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there</p>
                    <div class="span-6 serv">
                        <div class="icon-white-big left icon95-white"></div>
                        <h5 class="left"><a href="index.html" class="service-tipsy north link" title='Web site from 199.99$'>Internet marketing</a></h5>
                        <p class="clear small-italic">Nam euismod auctor lectus in sodales. Fusce sed justo estarena. Tollite fit manibus individuationis omnibus civitas.</p>
                    </div>
                    <div class="span-6 serv last">
                        <div class="icon-white-big left icon58-white"></div>
                        <h5 class="left"><a href="index.html" class="service-tipsy north link" title='Some text'>Information protection</a></h5>
                        <p class="clear small-italic">Tollite fit manibus individuationis omnibus civitas. Nam euismod auctor lectus in sodales. Fusce sed justo est.</p>
                    </div>
                    <div class="span-6 serv">
                        <div class="icon-white-big left icon74-white"></div>
                        <h5 class="left"><a href="index.html" class="service-tipsy north link" title='Some subtitle'>Moving services</a></h5>
                        <p class="clear small-italic nobottommargin">Nam euismod auctor lectus in sodales. Fusce sed justo estarena. Iuraveras magnifice ex auxilium super.</p>
                    </div>
                    <div class="span-6 serv last">
                        <div class="icon-white-big left icon181-white"></div>
                        <h5 class="left"><a href="index.html" class="service-tipsy north link" title='Some text'>Some service title</a></h5>
                        <p class="clear small-italic nobottommargin">Nam euismod auctor lectus in sodales. Fusce sed justo est. Ut dapib us adipis. Iuraveras magnifice ex auxilium.</p>
                    </div>
            	</div>
	  			<div class="span-12 notopmargin why last">
                    <h3>Why choose <a class="link" href="">VisualGov</a> for you orginization?</h3>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't.</p>
                    <div class="span-12 last serv">
                        <div class="big-rounded-icon-white left margin15">
                            <div class="icon-white-big icon1-gray"></div>
                        </div>
                        <h5>Who are we?</h5>
                        <p class="notopmargin small-italic">Lorem ipsum dolor sit amet, tollite fit manibus individuationis omnibus civitas.</p>
                    </div>
                    <div class="span-12 last serv">
                        <div class="big-rounded-icon-white left margin15">
                            <div class="icon-white-big icon4-gray"></div>
                        </div>
                        <h5>Our Mission</h5>
                        <p class="notopmargin small-italic">Consectetur lorem ipsum dolor sit amet, adipiscing elit. Mauris a ipsum ut odio.</p>
                    </div>
                    <div class="span-12 last serv">
                        <div class="big-rounded-icon-white left margin15">
                            <div class="icon-white-big icon6-gray"></div>
                        </div>
                        <h5>Our Progress</h5>
                        <p class="small-italic">Morem ipsum dolor sit amet, tollite fit manibus individuationis omnibus civitas.</p>
                    </div>
            	</div>
            </div>
			<!-- FROM PORTFOLIO -->
            <div class="span-24 separator"></div>
            <div class="span-24 from_portfolio">
                <div class="span-24 notopmargin">
                    <div class="span-6 notopmargin">
                    	<h3>Awesome designs</h3>
                        <p>Lorem ipsum dolor sit amet, tollite fit manibus individuationis omnibus civitas ad quia.Amice suspiciosa mare non solutionem.<br/><br/>Tollite fit manibus individuationis omnibus civitas. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<p class="small-italic">Lorem Ipsum is simply dummy text of the printing and typesett-ing industry. Lorem Ipsum is simply dummy text ans more</p>
						<a class="button_readmore on_white"  href="portfolio-details.html"></a>
                    </div>
                    <div class="span-6 inner-6 view">
                        <div class="item"><a href="images/slide-1-1.jpg" class="item-preview spec-border-ie" rel="prettyPhoto" title=""><img class="img-preview spec-border"  src="images/gallery/4col-1-inner.jpg" alt=" "/></a></div>
                        <h5><a class="link" href="portfolio-details.html">Some Project name</a></h5>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                    	<a class="button_readmore"  href="portfolio-details.html"></a>
                    </div>
                    <div class="span-6 inner-6 view">
                        <div class="item"><a href="images/slide-1-1.jpg" class="item-preview spec-border-ie" rel="prettyPhoto" title=""><img class="img-preview spec-border"  src="images/gallery/4col-2-inner.jpg" alt=" "/></a></div>
                        <h5><a class="link" href="portfolio-details.html">Some Project name</a></h5>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                    	<a class="button_readmore"  href="portfolio-details.html"></a>
                    </div>
                    <div class="span-6 inner-6 view last">
                        <div class="item"><a href="images/slide-1-1.jpg" class="item-preview spec-border-ie" rel="prettyPhoto" title=""><img class="img-preview spec-border"  src="images/gallery/4col-3-inner.jpg" alt=" "/></a></div>
                        <h5><a class="link" href="portfolio-details.html">Some Project name</a></h5>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                    	<a class="button_readmore"  href="portfolio-details.html"></a>
                    </div>
                </div>
            </div>
			<div class="clear"></div>
        </div>
        <!-- END FROM PORTFOLIO -->
</asp:Content>
