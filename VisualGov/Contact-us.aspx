﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Contact-us.aspx.cs" Inherits="VisualGov.Contact_us" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <!-- WELCOME AREA -->
            <div class="span-24 slider-area-inner">
            	<h1 class="colored left">Contact us</h1>
                <p class="descr">: Were here to help!</p>
			</div>
            <div class="span-24 separator-inner"></div>
            <!-- END WELCOME AREA -->
            <!-- SUBPAGE CONTENT -->
            <div class="span-16">
                <div id="mapviewer"><div id="Div1"><iframe id="map" Name="mapFrame" scrolling="no" width="610" height="250" frameborder="0" src="http://www.bing.com/maps/embed/?lvl=15&amp;cp=28.059295~-82.64244500000001&amp;sty=r&amp;draggable=true&amp;v=2&amp;dir=0&amp;where1=13039+W+Linebaugh+Ave%2C+Tampa%2C+FL+33626&amp;form=LMLTEW&amp;pp=28.05929459631443~-82.64244452118873&amp;mkt=en-us&amp;emid=1de5b1d5-9c96-983a-f85e-bf97ba81a14b&amp;w=610&amp;h=250"></iframe><div id="LME_maplinks" style="line-height:20px;"><a id="LME_largerMap" href="http://www.bing.com/maps/?mm_embed=map&amp;cp=28.059295~-82.64244500000001&amp;lvl=15&amp;sty=r&amp;where1=13039+W+Linebaugh+Ave%2C+Tampa%2C+FL+33626&amp;form=LMLTEW" target="_blank">View Larger Map</a>&nbsp;<a id="LME_directions" href="http://www.bing.com/maps/?mm_embed=dir&amp;cp=28.059295~-82.64244500000001&amp;rtp=~pos.28.05929459631443_-82.64244452118873_13039+W+Linebaugh+Ave%2C+Tampa%2C+FL+33626&amp;lvl=15&amp;sty=r&amp;form=LMLTEW" target="_blank">Get Directions</a>&nbsp;<a id="LME_birdsEye" href="http://www.bing.com/maps/?mm_embed=be&amp;cp=28.059295~-82.64244500000001&amp;lvl=18&amp;sty=b&amp;where1=13039+W+Linebaugh+Ave%2C+Tampa%2C+FL+33626&amp;form=LMLTEW" target="_blank">View Bird's Eye</a></div></div></div></br></br>
            	<h3>Do you need help, support or need a price quote?</h3>
                <p>Please send us a message and will respond as soon as we can.</p>
            	<div class="span-16 inner-16 view notopmargin">
                <div id="note"></div>
                <fieldset class="info_fieldset">
                <div id="fields">
                <form id="ajax-contact-form" action="javascript:alert('Was send!');">
                <div class="span-8 form notopmargin">
               	  <input class="text" type="text" name="name" value="" placeholder="Name" required /><br />
                </div>
                <div class="span-8 form last notopmargin">
               	  <input class="text" type="text" name="email" value="" placeholder="E-mail" required /><br />
                </div>
                <div class="span-16 last form">
                	<input class="texts" type="text" name="subject" value="" placeholder="Subject" required /><br />
                </div>
                <div class="span-16 last">
                	<textarea class="text" name="message" rows="5" cols="25" placeholder="Message" required ></textarea><br />
                </div>
                <div class="span-12 notopmargin">
                	<input class="button" type="submit" name="submit" value="Send message" id="submit_form" required/>
                </div>
                </form>
                </div>
                </fieldset>
                </div>
            </div>
            <div class="span-8 notopmargin last">
            	
                <div class="span-8 last side-bar">
                    <h5>Our adress</h5>
                    <p class="margin20 nobottommargin">13039 Linebaugh Ave Suite 102 
                    <br/>Tampa, FL 33262
                    <br/>Phone: <span class="colored">(813) 925-3703</span>
                    <br/>Fax: <span class="colored">(813) 925-3733</span>
                    <br/>Website: <a class="link" href="">http://www.visualgov.com</a>
                    <br/>Email: <a class="link nobottommargin" href="">info@visualgov.com</a>
                    </p>
                </div>
                
                <div class="span-8 last side-bar">
                    <br /><br />
                    <h5>Brochure Download</h5>
                        <div class="big-rounded-icon-white left nobottommargin">
                            <div class="icon-white-big icon223-gray"></div>
                        </div>
                    <h6><a class="link" href="">Download our PDF Brochure</a></h6>
					<p class="notopmargin nobottommargin">To learn more about VisualGov please review our brouchure.</p>
                </div>
                <div class="span-8 last side-bar">
                    <br /><br />
                    <h5>Office Hours</h5>
                    <p>VisualGov is open from 8:00am - 7:00pm EST</p>
                </div>
            </div>
            <!-- END SUBPAGE CONTENT -->
            <div class="clear"></div>
        </div>
</asp:Content>
